PRODUCT_BRAND ?= android-generic

ifeq ($(PRODUCT_GMS_CLIENTID_BASE),)
OMNI_PRODUCT_PROPERTIES += \
    ro.com.google.clientidbase=android-google
else
OMNI_PRODUCT_PROPERTIES += \
    ro.com.google.clientidbase=$(PRODUCT_GMS_CLIENTID_BASE)
endif

# general properties
OMNI_PRODUCT_PROPERTIES += \
    ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
    ro.com.android.wifi-watchlist=GoogleGuest \
    ro.setupwizard.enterprise_mode=1 \
    ro.build.selinux=1

# Init script file with omni extras
PRODUCT_COPY_FILES += \
    vendor/android-generic/prebuilt/etc/init.local.rc:root/init.omni.rc

# permissions
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml


# Additional packages
-include vendor/android-generic/utils/emulator/packages.mk

WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY := true
